import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AudioPlayerService } from 'src/app/services/audio-player.service';

@Component({
    selector: 'app-player',
    templateUrl: './player.page.html',
    styleUrls: ['./player.page.scss'],
})
export class PlayerPage implements OnInit {

    @Input() data: any;
    dataAudio : any;
    buttonPlay = false;
    stream: any = null;
    valueVolume = 50;
    isPlaying:boolean=false;
    buttonDirect:boolean=true;
    src:string;
    constructor( public modalController: ModalController,
                private AudioPlayerService: AudioPlayerService ) { }

    ngOnInit() {
      this.AudioPlayerService.getIsPlaying().subscribe(
                isPlaying => {this.isPlaying = isPlaying}
            );
      this.dataAudio={title:this.AudioPlayerService.title,
                      text:this.AudioPlayerService.text,
                      photo:this.AudioPlayerService.photo};
      console.log(this.AudioPlayerService.isPlaying);
      if (this.isPlaying){
        this.buttonPlay=true;
      }
      this.valueVolume=this.AudioPlayerService.getAudio().volume*100;
      console.log("AudioPlayerService");
      this.AudioPlayerService.src.subscribe(
                src => {this.src = src}
            );
      if (this.AudioPlayerService.getAudio().src!="http://str2.creacast.com/radio_peinard"){
        this.buttonDirect=true;
      }
    }

    clickButtonMusic() {
    }

    closeModal() {
        this.modalController.dismiss();
    }

    play() {
        this.AudioPlayerService.playAudio();
        console.log("play audio");
        this.buttonPlay = true;

    }
    relancerDirect(){
      //this.AudioPlayerService.setAudio("http://str2.creacast.com/radio_peinard",true);
      this.buttonPlay = true;
      this.AudioPlayerService.photo="http://www.peinard.fr/visuel_slide/rps.png";
      this.AudioPlayerService.title="Radio peinard - 100.fm";
      this.AudioPlayerService.text="La radio locale";
      this.dataAudio={title:this.AudioPlayerService.title,
                      text:this.AudioPlayerService.text,
                      photo:this.AudioPlayerService.photo};
    }

    stop() {
        this.AudioPlayerService.pauseAudio();
        this.buttonPlay = false;

    }

    volumen(event) {

        this.valueVolume = event.detail.value;
        this.AudioPlayerService.setVolume(this.valueVolume / 100);

    }
}
