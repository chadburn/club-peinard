import { Injectable } from '@angular/core';
import { SplashScreen } from '@capacitor/splash-screen';

@Injectable({
    providedIn: 'root'
})
export class CustomSplashService {
    constructor() { }

    async showSplash() {
        await SplashScreen.show({
            autoHide: false
        })
    }

    async hideSplash() {
        await SplashScreen.hide();
    }

    async autoHideSplash(duration: number = 2000) {
        await SplashScreen.show({
            showDuration: duration,
            autoHide: true
        });
    }
}