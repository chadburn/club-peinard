import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HostItemComponent } from './host-item.component';

describe('HostItemComponent', () => {
  let component: HostItemComponent;
  let fixture: ComponentFixture<HostItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HostItemComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HostItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
