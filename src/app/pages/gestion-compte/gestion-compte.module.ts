import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GestionComptePageRoutingModule } from './gestion-compte-routing.module';

import { GestionComptePage } from './gestion-compte.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GestionComptePageRoutingModule
  ],
  declarations: [GestionComptePage]
})
export class GestionComptePageModule {}
