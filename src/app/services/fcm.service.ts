import { Injectable } from '@angular/core';
import { Capacitor } from '@capacitor/core';
import { LocalNotifications } from '@capacitor/local-notifications';
import {
  ActionPerformed,
  PushNotificationSchema,
  PushNotifications,
  Token,
} from '@capacitor/push-notifications';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class FcmService {
  tokenValue:any="Notifications désactivées";
  constructor(private router: Router) {}
  initPush() {
    if (Capacitor.getPlatform() !== 'web') {
      this.registerPush();

    } else {
      //alert('No notifications');
    }
  }
  localNotifCheck() {
    LocalNotifications.checkPermissions().then((result) => {
      LocalNotifications.requestPermissions().then((res) => {
        if (res && res.display && res.display === 'denied') {
        }
      });
    });
  }
  getToken(){
    alert('token');
    PushNotifications.addListener('registration', (token: Token) => {
      //alert('Push registration success, token: ' + token.value);
      return token.value;
    });
  }
  private registerPush() {
    LocalNotifications.checkPermissions().then((result) => {
      LocalNotifications.requestPermissions().then((res) => {
        if (res && res.display && res.display === 'denied') {
        }
      });
    });
    PushNotifications.requestPermissions().then((result) => {
      if (result.receive === 'granted') {
        // Register with Apple / Google to receive push via APNS/FCM
        PushNotifications.register();
        PushNotifications.createChannel({
          id: 'fcm_default_channel',
          name: 'Notification du Club Peinard',
          description: 'Notifications générales du Club Peinard',
          importance: 5,
          visibility: 1,
          lights: true,
          vibration: true,
        });
      } else {
        // Show some error
      }
    });

    // On success, we should be able to receive notifications
    PushNotifications.addListener('registration', (token: Token) => {
      alert('Push registration success, token: ' + token.value);
      this.tokenValue=token.value;
    });

    // Some issue with our setup and push will not work
    PushNotifications.addListener('registrationError', (error: any) => {
      alert('Error on registration: ' + JSON.stringify(error));
    });

    // Show us the notification payload if the app is open on our device
    PushNotifications.addListener(
      'pushNotificationReceived',
      (notification: PushNotificationSchema) => {
        //alert('Push received: ' + JSON.stringify(notification));
      }
    );

    // Method called when tapping on a notification
    PushNotifications.addListener(
      'pushNotificationActionPerformed',
      (notification: ActionPerformed) => {
        //alert('Push action performed: ' + JSON.stringify(notification));
      }
    );
  }
}
