import { Component, OnInit, OnDestroy  } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { ModalController } from '@ionic/angular';
import { ReglementPage } from '../../pages/reglement/reglement.page';
import { GestionComptePage } from '../../pages/gestion-compte/gestion-compte.page';
import { FcmService } from '../../services/fcm.service';

@Component({
  selector: 'app-about',
  templateUrl: 'about.page.html',
  styleUrls: ['about.page.scss']
})
export class AboutPage {
connected:boolean=false;
fcmToken:any="Notifications désactivées";

  constructor(private http: HttpClient,public modalController: ModalController,private fcmService: FcmService) {}
  ngOnInit() {
    let id=localStorage.getItem('idadh');
    if (id){
      this.connected=true;
    }
    this.fcmToken=this.fcmService.tokenValue;
  }
  ionViewDidEnter(){
    let id=localStorage.getItem('idadh');
    if (id){
      this.connected=true;
    }
    else {
      this.connected=false;
    }
  }
desinscription() {
  if(confirm("Si vous validez, votre compte sera supprimé automatiquement dans 15 jours.")) {
    let carte=localStorage.getItem('idcarte');
    let id=localStorage.getItem('idadh');
    const headers = new HttpHeaders({
   		'Content-Type': 'application/x-www-form-urlencoded'
   	});
   	const postData: any = {id:id,carte:carte};
   	let url="https://app.clubpeinard.fr/connecteurs/unsubscribe.php";
   	this.http
   		.post(
   			url,
   			postData
   		)
   		.subscribe(response => {
        console.log(response);
   			if (response['success']){
          this.connected=false;
          localStorage.removeItem('idadh');
          localStorage.removeItem('idcarte');
          localStorage.removeItem('nomadh');
          localStorage.removeItem('prenomadh');
          alert("Votre compte à bien été supprimé");
   			}
   			else {

   			}
   		});
 }
}
async affichReglement(){
  console.log('affich reglement');
  const modal = await this.modalController.create({
      component: ReglementPage
  });
  return await modal.present();
  }

  async affichGestionCompte(){
      const modal = await this.modalController.create({
        component: GestionComptePage
    });
    return await modal.present();
  }
}
