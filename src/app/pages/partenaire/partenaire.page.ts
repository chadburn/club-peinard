import { Component, OnInit, Input  } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { ReglementPage } from '../../pages/reglement/reglement.page';


@Component({
    selector: 'app-partenaire',
    templateUrl: './partenaire.page.html',
    styleUrls: ['./partenaire.page.scss'],
})
export class PartenairePage implements OnInit {
  connected:boolean=false;
  adherent:any={};
  jeuDetails:any={dejajoue:true,gagnants:[]};
  login:any;
  returnMessage:any;
  remerciements:any="Chargement, veuillez patienter...";
  idCarte:any;
  jeuIdCarte:any;
  disableJouerButton : boolean=true;
  @Input() nom:string;
  @Input() lieu:string;
  @Input() date:string;
  @Input() typeJeu:string;
  @Input() commentaire:string;
  @Input() info:string;
  @Input() image:any;
  @Input() id:number;
  private requestHeaders: HttpHeaders;
    constructor( public modalController: ModalController,private http: HttpClient ) {
      this.requestHeaders = new HttpHeaders();
        this.requestHeaders.append('Content-Type', 'application/json');
    }

    ngOnInit() {
      let id=localStorage.getItem('idadh');
      this.idCarte=localStorage.getItem('idcarte');
      console.log(this.idCarte);
      if (id){
        console.log("connected");
        this.connected=true;
        let nom=localStorage.getItem('nomadh');
        let prenom=localStorage.getItem('prenomadh');
        this.adherent={id:id,nom:nom,prenom:prenom};
      }
      this.getDetails();
    }

    clickButtonMusic() {
    }
    getDetails(){
      console.log("getdetails");
      const postData: any = {
            id_jeu:this.id,
            id_adh:this.adherent.id,
            type:this.typeJeu};
      console.log(postData);
    let url = "https://app.clubpeinard.fr/connecteurs/jeux.php?action=getdetailsjeu";
    this.http.post(url,postData,{headers: this.requestHeaders}).subscribe((res:any) => {
      console.log(res);
      this.jeuDetails=res;
      this.remerciements="Merci d'avoir participé à ce jeu !";

      this.disableJouerButton=false;
    })
    }
    participer(jeu){
      console.log(jeu);
      // this.disableJouerButton=true;
       // if (jeu=='standard'){
       //  const postData: any = {
       //        id_jeu:this.id,
       //        id_adh:this.adherent.id};
       //  console.log("toto");
       // }else{
        const postData: any = {
              id_jeu:this.id,
              id_adh:this.adherent.id,
              type:jeu};
       // }

      let url = "https://app.clubpeinard.fr/connecteurs/jeux.php?action=play";
      this.http.post(url,postData,{headers: this.requestHeaders}).subscribe((res:any) => {
        console.log(res);
        this.getDetails();
      },
          (error) => {                              //Error callback
            alert('Veuillez vérifier votre connection Internet')
            //throw error;   //You can also throw the error to a global error handler
          });
    }

    closeModal() {
        this.modalController.dismiss();
    }
removeUser(){
  this.connected=false;
  this.adherent={};
  localStorage.removeItem('idadh');
  localStorage.removeItem('nomadh');
  localStorage.removeItem('prenomadh');
}
onSubmit(data) {
  const headers = new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded'
  });
  const postData: any = data;
  let url="https://app.clubpeinard.fr/connecteurs/login/auth2.php?email="+data.email;
  this.http
    .post(
      url,
      postData
    )
    .subscribe(response => {
      if (response['success']){
        this.connected=true;
        this.adherent=response['adherent'];
        localStorage.setItem('idadh', this.adherent.id);
        localStorage.setItem('idcarte', this.adherent.idCarte);
        localStorage.setItem('nomadh', this.adherent.nom);
        localStorage.setItem('prenomadh', this.adherent.prenom);
      }
      else {
        this.returnMessage="Vous n'êtes pas inscrit au club peinard.";
      }
    },
        (error) => {                              //Error callback
          alert('Veuillez vérifier votre connection Internet')
          //throw error;   //You can also throw the error to a global error handler
        });
  console.log(data);
}
openUrl(url){
  console.log("url");
  console.log(url);
  window.open(url,'_blank');
}
async affichReglement(){
  console.log('affich reglement');
  const modal = await this.modalController.create({
      component: ReglementPage
  });
  return await modal.present();
  }
}
