import { Component, OnInit, OnDestroy } from '@angular/core';
//import { transition, trigger, style, animate, query, stagger, animateChild, state } from '@angular/animations';
import { ModalController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
import { CustomSplashService } from 'src/app/services/splash-service';
import { Badge } from '@ionic-native/badge/ngx';
import { FcmService } from 'src/app/services/fcm.service';

@Component({
	selector: 'app-partenaires',
	templateUrl: './partenaires.page.html',
	styleUrls: ['./partenaires.page.scss']
})
export class PartenairesPage {
	jeux: any;
	jeuxCarteH1: any;
	dataLoaded: boolean = true;
	lastUpdate:any;
	totAdherents:number=34000;
	annonce:string;
	idCarte:any;
	connected=false;

	constructor(private http: HttpClient, public splashService: CustomSplashService, private badge: Badge, private fcmService: FcmService) { }
	ngOnInit() {
		this.splashService.hideSplash();
		this.getListCarteHaut1();
		this.getList();
		this.getOther();
		this.updateBadge();
		setInterval(()=> { this.checkUptodate() }, 10000);
		this.fcmService.initPush();
		this.fcmService.localNotifCheck();

	}
	ionViewDidEnter(){
		console.log('ionview');
		let id=localStorage.getItem('idadh');
		this.idCarte=localStorage.getItem('idcarte');
		if (id){
			this.connected=true;
		}
		else {
			this.connected=false;
		}
	}
checkUptodate(force=0){
	let difference=Date.now()-this.lastUpdate;

	if (difference>43200000 || force)
	{
		console.log("reload");
		this.getList();
		this.getListCarteHaut1();
		this.getOther();
	}
}
	getListCarteHaut1() {
		console.log("getListCarteHaut1");
		let url = "https://app.clubpeinard.fr/connecteurs/jeux.php?action=list_jeucarte";
		this.http.get(url).subscribe((res) => {
			if (res) {
				this.jeuxCarteH1 = res;
			}
			console.log(this.jeuxCarteH1)
		})
	}
	getOther() {
		console.log('global params');
		let url = "https://app.clubpeinard.fr/connecteurs/other.php";
		this.http.get(url).subscribe((res) => {
			if (res) {
				console.log(res);
				this.totAdherents = res['nb_adh'];
				this.annonce = res['annonce'];
			}
		})
	}
	getList() {
		this.dataLoaded = false;

		let url = "https://app.clubpeinard.fr/connecteurs/jeux.php?action=list";
		this.http.get(url).subscribe((res) => {
			if (res) {
				console.log("getlist");
				this.jeux = res;
				this.lastUpdate=Date.now();
			}
			this.dataLoaded = true;
			console.log(this.jeux)
		},
				(error) => {
					if (!this.jeux){
						this.jeux=[];
					}
					this.lastUpdate=Date.now();
					this.dataLoaded=true;                          //Error callback
					alert('Veuillez vérifier votre connexion Internet et rechargez l\'appli')
					//throw error;   //You can also throw the error to a global error handler
				});
	}
	filterJeuCarte(val){
		return this.jeuxCarteH1.filter(x => x.haut == val);
	}

	public updateBadge() {
	   // if(currentBadgeCount === 0) {
	      this.badge.clear();
	   // }
	}
	goTo(url){
	  //console.log("url");
	  //console.log(url);
	  window.location.replace(url);
	}
	// getListCarteHaut0() {
	// 	console.log("getListCarteHaut0");
	// 	let url = "https://app.clubpeinard.fr/connecteurs/jeux.php?action=list_jeucarte";
	// 	this.http.get(url).subscribe((res) => {
	// 		if (res) {
	// 			this.jeuxCarteH0 = res;
	// 			this.lastUpdate=Date.now();
	// 		}
	// 		this.dataLoaded = true;
	// 		console.log(this.jeux)
	// 	})
	// }
}
