import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,FormGroup } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ConnexionPage } from './connexion.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { HostProfilePage } from '../host-profile/host-profile.page';
import { HostProfilePageModule } from '../host-profile/host-profile.module';

const routes: Routes = [
  {
    path: '',
    component: ConnexionPage
  }
];

@NgModule({
  entryComponents: [
    HostProfilePage
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    HostProfilePageModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ConnexionPage]
})
export class ConnexionPageModule {}
