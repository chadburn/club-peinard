import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { ModalController } from '@ionic/angular';
import { PlayerPage } from '../player/player.page';
import { Router } from '@angular/router';
import { NotificationsPage } from '../notifications/notifications.page';
import { HttpClient } from '@angular/common/http';
import { CustomSplashService } from 'src/app/services/splash-service';
import { AudioPlayerService } from 'src/app/services/audio-player.service';
import { Platform } from '@ionic/angular';
@Component({
  selector: 'app-radio',
  templateUrl: 'radio.page.html',
  styleUrls: ['radio.page.scss']
})
export class RadioPage implements OnInit {

  shows: any;
  emissions: any;
  dataLoaded: boolean = false;
  upcomingConf = {
    spaceBetween: 20,
    slidesPerView: 2,
    scrollbar: true
  };
  localPlatform: any = 'desktop';
  onTheAir =
    {
      hour: 'Direct',
      photo: 'http://www.peinard.fr/visuel_slide/rps.png',
      title: 'Radio Peinard',
      text: 'la radio locale'
    };

  constructor(private dataService: DataService,
    private router: Router,
    public modalController: ModalController,
    public splashService: CustomSplashService,
    private http: HttpClient, private AudioPlayerService: AudioPlayerService,
    public platform: Platform) { }

  ngOnInit() {
    this.emissions = { direct_t: false, interview: false, peinard_d: false };
    this.getShows();
    this.getEmissions();
    // this.AudioPlayerService.loadAudio();
    if (this.platform.is('ios')) {
      console.log('ios');
      this.localPlatform = 'ios';
    }
    if (this.platform.is('android')) {
      console.log('android');
      this.localPlatform = 'android';
    }
    if (this.platform.is('desktop')) {
      console.log('desktop');
      this.localPlatform = 'desktop';
    }

  }

  getShows() {
    this.shows = this.dataService.getShows();
  }

  getEmissions() {
    let url = 'http://app.clubpeinard.fr/connecteurs/peinard.php';
    this.http.get(url).subscribe((res) => {
      this.emissions = res;
      this.dataLoaded = true;
      console.log("radio.page.ts -> getEmissions()")
      console.log(this.emissions)
      this.splashService.hideSplash();
    }, (err) => {
      console.log("Error", JSON.stringify(err))
      this.splashService.hideSplash();
    })
  }

  async presentModal(info: any) {
    const modal = await this.modalController.create({
      component: PlayerPage,
      componentProps: { data: info }
    });
    return await modal.present();
  }

  async goToNotifications() {
    const modal = await this.modalController.create({
      component: NotificationsPage
    });

    return await modal.present();
  }
}
