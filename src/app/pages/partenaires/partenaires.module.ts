import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PartenairesPage } from './partenaires.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { PartenairePageModule } from '../partenaire/partenaire.module';
import { PartenairePage } from '../partenaire/partenaire.page';
import { InscriptionPageModule } from '../inscription/inscription.module';
import { InscriptionPage } from '../inscription/inscription.page';

const routes: Routes = [
  {
    path: '',
    component: PartenairesPage
  }
];

@NgModule({
  entryComponents: [
    PartenairePage
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    PartenairePageModule,
    InscriptionPageModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PartenairesPage]
})
export class PartenairesPageModule {}
