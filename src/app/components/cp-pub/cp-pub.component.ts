import { Component, OnInit } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import Swiper from 'swiper';
import SwiperCore, { Autoplay } from 'swiper';

SwiperCore.use([Autoplay]);

@Component({
  selector: 'app-cp-pub',
  templateUrl: './cp-pub.component.html',
  styleUrls: ['./cp-pub.component.scss'],
})
export class CpPubComponent implements OnInit {
adsList:any=[];
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getList();
  }
  getList(){
    const postData: any = {};
  let url = "https://app.clubpeinard.fr/connecteurs/ads.php";
  this.http.get(url)
  .subscribe(res => {
  console.log(res);
  this.adsList=res;
  })
  }

  setSwiperInstance(swiper: Swiper) {
  setInterval(() => {
    //console.log(!swiper.isEnd);
    //console.log(swiper);
    if (!swiper.isEnd){
      //console.log (swiper);
      swiper.slideNext();
      setInterval(() => {
          console.log('back');
            swiper.slideToLoop(0);
          }, 5000);
    }
    else {
      swiper.slideNext();
    }
  }, 5000);
}

}
