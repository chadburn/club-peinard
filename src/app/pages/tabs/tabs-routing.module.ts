import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'rps',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../radio/radio.module').then(m => m.RadioPageModule)
          }
        ]
      },
      {
        path: 'shedule',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../schedule/schedule.module').then(m => m.SchedulePageModule)
          }
        ]
      },
      {
        path: 'partenaires',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../partenaires/partenaires.module').then(m => m.PartenairesPageModule)
          }
        ]
      },
      {
        path: 'jackpot',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../jackpot/jackpot.module').then(m => m.JackpotPageModule)
          }
        ]
      },
      {
        path: 'carte',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../carte/carte.module').then(m => m.CartePageModule)
          }
        ]
      },
      {
        path: 'settings',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../settings/settings.module').then(m => m.SettingsPageModule)
          }
        ]
      },
      {
        path: 'about',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../about/about.module').then(m => m.AboutPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/partenaires',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/partenaires',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
