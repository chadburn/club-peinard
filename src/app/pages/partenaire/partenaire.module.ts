import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PartenairePage } from './partenaire.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { InscriptionPageModule } from '../inscription/inscription.module';
import { InscriptionPage } from '../inscription/inscription.page';



@NgModule({
  entryComponents: [
    InscriptionPage
  ],
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    InscriptionPageModule,
    IonicModule
  ],
  declarations: [PartenairePage]
})
export class PartenairePageModule {}
