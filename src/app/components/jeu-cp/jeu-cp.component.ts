import { Component, OnInit,Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { PartenairePage } from 'src/app/pages/partenaire/partenaire.page';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient,HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-jeu-cp',
  templateUrl: './jeu-cp.component.html',
  styleUrls: ['./jeu-cp.component.scss'],
})
export class JeuCpComponent implements OnInit {

  @Input() nom:string;
  @Input() lieu:string;
  @Input() date:string;
  @Input() places:string;
  @Input() typeJeu:string;
  @Input() commentaire:string;
  @Input() info:string;
  @Input() image:any;
  @Input() id:number;
  termine:boolean=false;
  gagnant:boolean=false;
  idAdh:any;

  constructor(private sanitizer: DomSanitizer, public modalController: ModalController,private http: HttpClient ) {
  this.image = sanitizer.bypassSecurityTrustStyle(this.image); }

  ngOnInit() {
    this.getStatus();
  }
  async presentModal() {
    const modal = await this.modalController.create({
        component: PartenairePage,
        componentProps: { nom: this.nom,
                          lieu: this.lieu,
                          image: this.image,
                          places: this.places,
                          typeJeu: this.typeJeu,
                          commentaire: this.commentaire,
                          info: this.info,
                          id: this.id,
                          date: this.date
                        }
    });
    return await modal.present();
  }
  getStatus(){
    const postData: any = {
          id_adh:localStorage.getItem('idadh'),
          id_jeu:this.id};
  let url = "https://app.clubpeinard.fr/connecteurs/jeux.php?action=getStatusJeu";
  this.http.post(url,postData)
  .subscribe(res => {
    this.termine=res['termine'];
    this.gagnant=res['gagnant'];
  })
  }
  goTo(id) {
    window.open("http://www.clubpeinard.com/?page=2#"+this.id,'_blank');
        // Opening a URL and returning an InAppBrowserObject
       // Inject scripts, css and more with browser.X
      }
  openUrl(url){
    console.log("url");
    console.log(url);
    window.open(url,'_blank');
  }
}
