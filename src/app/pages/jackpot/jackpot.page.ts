import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-jackpot',
  templateUrl: './jackpot.page.html',
  styleUrls: ['./jackpot.page.scss'],
})
export class JackpotPage implements OnInit {
  connected:boolean=false;
  adherent:any={};
  idCarte:any;
  returnMessage:any;
  audiostop = new Audio('../../../assets/jackpot/stop.mp3');
  lancementtirage = false;
  resultattirage:any;
  credits=0;
  btndisabled=true;
  btnContent:string="patientez";
  audio:any;
  resulttop1:any;
  resulttop2:any;
  resulttop3:any;
  msg:any;
  top1=0;
  top2=0;
  top3=0;
  vitesse1=50;
  vitesse2=45;
  vitesse3=55;
  animate1:any;
  animate2:any;
  animate3:any;
  imgObj1:any;
  imgObj2:any;
  imgObj3:any;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    let id=localStorage.getItem('idadh');
    this.idCarte=localStorage.getItem('idcarte');
    console.log(this.idCarte);
    if (id){
      console.log("connected");
      this.connected=true;
      let nom=localStorage.getItem('nomadh');
      let prenom=localStorage.getItem('prenomadh');
      this.adherent={id:id,nom:nom,prenom:prenom};
    }
    this.initJackpot()
  }
  initJackpot(){

    this.imgObj1 = document.getElementById('slots1');
    this.imgObj2 = document.getElementById('slots2');
    this.imgObj3 = document.getElementById('slots3');
    console.log(this.imgObj3);
    let url='https://app.clubpeinard.fr/connecteurs/jackpot/callback.php?id_adh='+localStorage.getItem('idadh')+'&ask=affichslot';
    console.log(url);
    this.http.get(url).subscribe((res) => {
      if (res['credits']>0){
        this.btndisabled=false;
        this.btnContent="JOUER";
      }
      else if(res['credits']==0){
        this.btnContent="à demain";
        this.msg="Bénéficiez de 5 nouveaux crédits demain 🙂";

      }
      console.log(res);
      this.credits=res['credits'];
      this.resulttop1=res['r1'];
      this.resulttop2=res['r2'];
      this.resulttop3=res['r3'];
      this.imgObj1.style.top =res['r1']+"px";
      this.imgObj2.style.top =res['r2']+"px";
      this.imgObj3.style.top =res['r3']+"px";
    })
  }

  start(){
    this.btndisabled = true;
    this.btnContent ="Patientez...";
    console.log('tirage');
    let url='https://app.clubpeinard.fr/connecteurs/jackpot/callback.php?id_adh='+localStorage.getItem('idadh')+'&ask=tirage&from=appli';
    this.http.get(url).subscribe((res) => {
      console.log(res);
      let top1=this.resulttop1=res['r1'];
      let top2=this.resulttop2=res['r2'];
      let top3=this.resulttop3=res['r3'];
      if (res) {
        this.resultattirage=res;
        this.resulttop1=res['r1'];
        this.resulttop2=res['r2'];
        this.resulttop3=res['r3'];
        //this.imgObj1.style.top =res['r1']+"px";
        //this.imgObj1.style.top =res['r1']+"px";
        //this.imgObj1.style.top =res['r1']+"px";
        //this.msg=res['msg'];
        if(isNaN(res['credits']) == true) {
          this.credits=0;
        }
        else {
          this.credits=res['credits'];
        }
        if (this.resultattirage!="false" && isNaN(this.resulttop1)==false && isNaN(this.resulttop2)==false && isNaN(this.resulttop3)==false) {
           this.move1(top1,this.imgObj1,res['r1'],0);
           this.move2(top2,this.imgObj2,res['r2'],0);
           this.move3(top3,this.imgObj3,res['r3'],0,this.credits,res['msg']);

        this.playAudio();
        //document.getElementById("audioslots").play();
        }
        else {
        this.btndisabled = true;
        this.btnContent ="à demain";

        //document.getElementById("credits").innerHTML='0';

        }
      }
      else {
        this.lancementtirage=false;
        this.resultattirage=res;
      }
      res=this.resultattirage;
    })
  }

  move1(top,imgObj,resulttop,count){
    top=top-20;
  if (top<-595)
    top=55;
  else
    imgObj.style.top = top+'px';
  count+=1;
  var stoptop1=resulttop-40;
  var stoptop2=resulttop+40;
  if (count>100 && stoptop1<top && top<stoptop2) {
  imgObj.style.top =resulttop+"px";
  this.playStop();
  }
  else {
    imgObj.style.top =top+"px";
    let that = this;
    setTimeout(()=>{that.move1(top,imgObj,resulttop,count)},this.vitesse1);
  }

      };
  ///////////////////////////////////////////////////////Fonction Tirage 2

  move2(top,imgObj,resulttop,count){
    top=top-20;
    if (top<-595)
      top=55;
    else
      imgObj.style.top = top+'px';
  count+=1;
  var stoptop1=resulttop-40;
  var stoptop2=resulttop+40;
  if (count>75 && stoptop1<top && top<stoptop2) {
  imgObj.style.top =resulttop+"px";
  this.playStop();
  }
  else {
    let that = this;
    let animate2 = setTimeout(function(){that.move2(top,imgObj,resulttop,count);},this.vitesse2);
  }

      };

  move3(top,imgObj,resulttop,count,credits,msg){
    top=top-20;
    if (top<-595)
      top=55;
    else
      imgObj.style.top = top+'px';
        count+=1;
        var stoptop1=resulttop-40;
        var stoptop2=resulttop+40;
        if (count>125 && stoptop1<top && top<stoptop2){
        imgObj.style.top =resulttop+"px";
        this.credits =credits;
        this.pauseAudio();
        this.playStop();
        console.log('stop audio')
        this.credits =credits;
        //document.getElementById("message").innerHTML =msg;
        console.log("set message")
        this.msg=msg;
        if (credits>0) {
        this.btndisabled = false;
        this.btnContent ="Jouer";
        }
        else {
        this.btnContent ="à demain";
        }
        }
        else {
          let that = this;
          let animate3 = setTimeout(function(){that.move3(top,imgObj,resulttop,count,credits,msg);},this.vitesse3);
        }
      };


  stop(){
    this.audiostop = new Audio('../../../assets/jackpot/stop.mp3');
     setTimeout(function(){clearTimeout(this.animate1);this.audiostop.play();},1000);
     setTimeout(function(){clearTimeout(this.animate2);this.audiostop.play();},5000);
     setTimeout(function(){clearTimeout(this.animate3);this.audiostop.play();this.btndisabled = false;this.btnContent ="Jouer";},9000);

  };
  playAudio(){
    this.audio = new Audio();
    this.audio.src = "../../../assets/jackpot/slot.mp3";
    this.audio.load();
    this.audio.play();
  }
  playStop(){
    this.audiostop = new Audio();
    this.audiostop.src = "../../../assets/jackpot/stop.mp3";
    this.audiostop.load();
    this.audiostop.play();
  }
  pauseAudio(){
    this.audio.pause()
  }

  onSubmit(data) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    const postData: any = data;
    let url="https://app.clubpeinard.fr/connecteurs/login/auth2.php?email="+data.email;
    this.http
      .post(
        url,
        postData
      )
      .subscribe(response => {
        if (response['success']){
          this.connected=true;
          this.adherent=response['adherent'];
          localStorage.setItem('idadh', this.adherent.id);
          localStorage.setItem('idcarte', this.adherent.idCarte);
          localStorage.setItem('nomadh', this.adherent.nom);
          localStorage.setItem('prenomadh', this.adherent.prenom);
          this.initJackpot();
        }
        else {
          this.returnMessage="Vous n'êtes pas inscrit au club peinard.";
        }
      },
          (error) => {                              //Error callback
            alert('Veuillez vérifier votre connection Internet')
            //throw error;   //You can also throw the error to a global error handler
          });
    console.log(data);
  }

}
