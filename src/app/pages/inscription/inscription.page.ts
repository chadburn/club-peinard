import { Component, OnInit, Input  } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { HttpClient,HttpHeaders } from '@angular/common/http';


@Component({
    selector: 'app-inscription',
    templateUrl: './inscription.page.html',
    styleUrls: ['./inscription.page.scss'],
})
export class InscriptionPage implements OnInit {
  connected:boolean=false;
  adherent:any={};
  jeux:any;
  dataLoaded:boolean=true;
  login:any;
  returnMessage:any;
  idCarte:any;
  subscribeResponse:any={
                      errors:{
                        adresse: false,
                        cp: false,
                        email: false,
                        naissance: false,
                        nom: false,
                        prenom: false,
                        telephone: false,
                        ville: false
                      }
                          };
  private requestHeaders: HttpHeaders;
  // @Input() nom:string;
  // @Input() lieu:string;
  // @Input() date:string;
  // @Input() image:any;
  // @Input() id:number;

  constructor( public modalController: ModalController,private http: HttpClient ) {
    this.requestHeaders = new HttpHeaders();
      this.requestHeaders.append('Content-Type', 'application/json');
  }

  ngOnInit() {
    let id=localStorage.getItem('idadh');
    if (id){
      console.log("connected");
      this.connected=true;
      let nom=localStorage.getItem('nomadh');
      let prenom=localStorage.getItem('prenomadh');
      this.adherent={id:id,nom:nom,prenom:prenom};
    }
  }


  closeModal() {
      this.modalController.dismiss();
  }

  onSubmit(data) {
    this.subscribeResponse.errors={};
    const postData = JSON.stringify(data);
    // if(data.naissance){
    //   const date = data.naissance.substr(8,2) + "/" + data.naissance.substr(5,2) + "/" + data.naissance.substr(0,4);
    //   data.naissance=date;
    // }

    let url = "https://app.clubpeinard.fr/connecteurs/signup.php?";
    this.http
        .post(
          url,
          data,
          {headers: this.requestHeaders})
        .subscribe(response => {
          //console.log(response);
          this.subscribeResponse=response;
          console.log(this.subscribeResponse);
          if (response['success']){
            this.modalController.dismiss();
            this.connected=true;
            this.adherent=this.subscribeResponse.adherent;
            localStorage.setItem('idadh', this.adherent.id);
            localStorage.setItem('idcarte', this.adherent.idCarte);
            localStorage.setItem('nomadh', this.adherent.nom);
            localStorage.setItem('prenomadh', this.adherent.prenom);
            window.location.assign('/tabs/carte');
          }
          else {
            // this.returnMessage="";
            // console.log(response['errors']);
            // for (var index in response['errors']) {
            //   this.returnMessage=this.returnMessage + "\n" + response['errors'][index];
            //   console.log(this.returnMessage);
            // }

          }
        })
  }
}
