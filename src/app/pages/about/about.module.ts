import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AboutPage } from './about.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { PlayerPageModule } from '../player/player.module';
import { PlayerPage } from '../player/player.page';
import { InscriptionPageModule } from '../inscription/inscription.module';
import { InscriptionPage } from '../inscription/inscription.page';

const routes: Routes = [
  {
    path: '',
    component: AboutPage
  }
];

@NgModule({
  entryComponents: [
    PlayerPage,
    InscriptionPage
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    PlayerPageModule,
    InscriptionPageModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AboutPage]
})
export class AboutPageModule {}
