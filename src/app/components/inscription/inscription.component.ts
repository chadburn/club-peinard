import { Component, OnInit,Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InscriptionPage } from 'src/app/pages/inscription/inscription.page';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.scss'],
})
export class InscriptionComponent implements OnInit {

  // @Input() nom:string;
  // @Input() lieu:string;
  // @Input() date:string;
  // @Input() image:any;
  // @Input() id:number;

  constructor(private sanitizer: DomSanitizer, public modalController: ModalController) {
  // this.image = sanitizer.bypassSecurityTrustStyle(this.image); 
  }

  ngOnInit() {}
  async presentModal() {
    const modal = await this.modalController.create({
        component: InscriptionPage,
        componentProps: { 
                          // nom: this.nom,
                          // lieu: this.lieu,
                          // image: this.image,
                          // id: this.id,
                          // date: this.date
                        }
    });
    return await modal.present();
  }
  goTo(id) {
    window.open("http://www.clubpeinard.com/?page=2#5293",'_blank');
        // Opening a URL and returning an InAppBrowserObject
       // Inject scripts, css and more with browser.X
      }
}
