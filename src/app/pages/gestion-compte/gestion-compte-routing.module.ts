import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GestionComptePage } from './gestion-compte.page';

const routes: Routes = [
  {
    path: '',
    component: GestionComptePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GestionComptePageRoutingModule {}
