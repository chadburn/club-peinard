import { Component, OnInit, OnDestroy } from '@angular/core';
//import { transition, trigger, style, animate, query, stagger, animateChild, state } from '@angular/animations';
import { ModalController } from '@ionic/angular';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';

@Component({
	selector: 'app-carte',
	templateUrl: './carte.page.html',
	styleUrls: ['./carte.page.scss']
})
export class CartePage  {
jeux:any;
dataLoaded:boolean=true;
connected:boolean=false;
login:any;
returnMessage:any;
adherent:any={};
idCarte:any;
	constructor(private http: HttpClient) { }
	ngOnInit() {
		let id=localStorage.getItem('idadh');
		if (id){
			console.log("connected");
			this.connected=true;
			let nom=localStorage.getItem('nomadh');
			let prenom=localStorage.getItem('prenomadh');
			this.idCarte=localStorage.getItem('idcarte');
			this.adherent={id:id,nom:nom,prenom:prenom};

		}
	}
removeUser(){
	this.connected=false;
	this.adherent={};
	localStorage.removeItem('idadh');
	localStorage.removeItem('idcarte');
	localStorage.removeItem('nomadh');
	localStorage.removeItem('prenomadh');
}
onSubmit(data) {
	const headers = new HttpHeaders({
		'Content-Type': 'application/x-www-form-urlencoded'
	});
	const postData: any = data;
	let url="https://app.clubpeinard.fr/connecteurs/login/auth2.php?email="+data.email;
	this.http
		.post(
			url,
			postData
		)
		.subscribe(response => {
			if (response['success']){
				this.connected=true;
				console.log(response)
				this.adherent=response['adherent'];
				this.idCarte=this.adherent.idCarte;
				localStorage.setItem('idadh', this.adherent.id);
				localStorage.setItem('idcarte', this.adherent.idCarte);
				localStorage.setItem('nomadh', this.adherent.nom);
				localStorage.setItem('prenomadh', this.adherent.prenom);

			}
			else {
				this.returnMessage="Vous n'êtes pas inscrit au club peinard.";
			}
		});
  console.log(data);
}

}
