import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { ShowDetailPageModule } from './pages/show-detail/show-detail.module';
import { CartePrivilegePageModule } from './pages/carte-privilege/carte-privilege.module';
import { ConnexionPageModule } from './pages/connexion/connexion.module';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./pages/tabs/tabs.module').then(m => m.TabsPageModule)
      //import('./pages/jackpot/jackpot.module').then( m => m.JackpotPageModule)
  },
  {
    path: 'partenaire/:id',
    loadChildren: () =>
      import('./pages/show-detail/show-detail.module').then(
        m => ShowDetailPageModule
      )
  },
  {
    path: 'carte-privilege',
    loadChildren: () =>
      import('./pages/carte-privilege/carte-privilege.module').then(
        m => CartePrivilegePageModule
      )
  },
  {
    path: 'connexion',
    loadChildren: () =>
      import('./pages/connexion/connexion.module').then(
        m => ConnexionPageModule
      )
  },  {
    path: 'jackpot',
    loadChildren: () => import('./pages/jackpot/jackpot.module').then( m => m.JackpotPageModule)
  },
  {
    path: 'reglement',
    loadChildren: () => import('./pages/reglement/reglement.module').then( m => m.ReglementPageModule)
  },
  {
    path: 'gestion-compte',
    loadChildren: () => import('./pages/gestion-compte/gestion-compte.module').then( m => m.GestionComptePageModule)
  }




];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
