import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { HttpClient,HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-gestion-compte',
  templateUrl: './gestion-compte.page.html',
  styleUrls: ['./gestion-compte.page.scss'],
})
export class GestionComptePage implements OnInit {
  idCarte:any;
  adherent:any={};
  connected:boolean=false;
  private requestHeaders: HttpHeaders;

  constructor( public modalController: ModalController,private http: HttpClient ) {
    this.requestHeaders = new HttpHeaders();
      this.requestHeaders.append('Content-Type', 'application/json');
     }

  ngOnInit() {
    let id=localStorage.getItem('idadh');
    this.idCarte=localStorage.getItem('idcarte');
    console.log(this.idCarte);
    if (id){
      console.log("connected");
      this.connected=true;
      let nom=localStorage.getItem('nomadh');
      let prenom=localStorage.getItem('prenomadh');
      this.adherent={id:id,nom:nom,prenom:prenom};
  }
}

  getDetails(){
    console.log("getdetails");
    const postData: any = {
          id_adh:this.adherent.id,};
    console.log(postData);
  let url = "https://app.clubpeinard.fr/connecteurs/jeux.php?action=getdetailsjeu";
  this.http.post(url,postData,{headers: this.requestHeaders}).subscribe((res:any) => {
    console.log(res);

  })
  }
}
