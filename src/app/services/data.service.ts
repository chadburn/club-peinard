import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class DataService {
    public emissions = [];
    hosts = [
        {
            id: 1,
            avatar: 'https://images.unsplash.com/photo-1463453091185-61582044d556?ixlib=rb-1.2.1&auto=format&fit=crop&w=128&q=80',
            name: 'John Sanchez',
            bio: 'John is journalist with extrem profesionalism and always in the seek of truth. Her main goal is informing their peers',
            social: [
                {
                    id: 1,
                    type: 'twitter',
                    url: 'https://twitter.com/mobilethemes_'
                },
                {
                    id: 2,
                    type: 'linkedin',
                    url: 'https://linkedin.com/mobilethemes_'
                },
                {
                    id: 3,
                    type: 'web',
                    url: 'https://gumroad.com/mobile_themes'
                }
            ]
        },
        {
            id: 2,
            avatar: 'https://images.unsplash.com/photo-1568967729548-e3dbad3d37e0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=128&q=80',
            name: 'Lisa Martinez',
            bio: 'Lisa is journalist with extrem profesionalism and always in the seek of truth. Her main goal is informing their peers',
            social: [
                {
                    id: 1,
                    type: 'twitter',
                    url: 'https://twitter.com/mobilethemes_'
                },
                {
                    id: 2,
                    type: 'linkedin',
                    url: 'https://linkedin.com/mobilethemes_'
                },
                {
                    id: 3,
                    type: 'web',
                    url: 'https://gumroad.com/mobile_themes'
                }
            ]
        }
    ];

    episodes = [
        {
            id: 25,
            number: '25',
            title: 'John Sena is in da house',
            date: 'Sept 25, 2019',
            duration: '25:00'
        },
        {
            id: 24,
            number: '24',
            title: 'John Sena is in da house',
            date: 'Sept 20, 2019',
            duration: '25:00'
        }, {
            id: 23,
            number: '23',
            title: 'John Sena is in da house',
            date: 'Sept 15, 2019',
            duration: '25:30'
        }, {
            id: 22,
            number: '22',
            title: 'John Sena is in da house',
            date: 'Sept 10, 2019',
            duration: '35:00'
        }
    ];

    shows = [
        {
            id: 1,
            image: 'http://www.peinard.fr/assets/img/jpj1.png',
            name: 'Peinard Déchainé',
            hours: '',
            description: 'L\'actualité locale vue de Béziers par Jorge',
            host_by: this.hosts,
            episodes: this.episodes,
        },
        {
            id: 2,
            image: 'https://images.unsplash.com/photo-1516575869513-3f418f8902ca?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
            name: 'FM Indie Pop',
            hours: 'Mondays at 7:30PM',
            description: 'You’re always in time for the punchline with every kind of comedy, including Kevin Hart’s Laugh Out Loud Radio and the Netflix is a Joke channel, which broadcasts stand-up.',
            host_by: this.hosts,
            episodes: this.episodes,
        },
        {
            id: 3,
            image: 'https://images.unsplash.com/photo-1483000805330-4eaf0a0d82da?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
            name: 'DJ in da house',
            hours: 'Mondays at 7:30PM',
            description: 'You’re always in time for the punchline with every kind of comedy, including Kevin Hart’s Laugh Out Loud Radio and the Netflix is a Joke channel, which broadcasts stand-up.',
            host_by: this.hosts,
            episodes: this.episodes,
        },
        {
            id: 4,
            image: 'https://images.unsplash.com/photo-1531651008558-ed1740375b39?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
            name: 'Tonight Show',
            hours: 'Mondays at 7:30PM',
            description: 'You’re always in time for the punchline with every kind of comedy, including Kevin Hart’s Laugh Out Loud Radio and the Netflix is a Joke channel, which broadcasts stand-up.',
            host_by: this.hosts,
            episodes: this.episodes,
        },
        {
            id: 5,
            image: 'https://images.unsplash.com/photo-1571942347102-23df2d0ee3a2?ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=80',
            name: 'Top Music playlist',
            hours: 'Mondays at 7:30PM',
            description: 'You’re always in time for the punchline with every kind of comedy, including Kevin Hart’s Laugh Out Loud Radio and the Netflix is a Joke channel, which broadcasts stand-up.',
            host_by: this.hosts,
            episodes: this.episodes,
        },
        {
            id: 6,
            image: 'https://images.unsplash.com/photo-1510074468346-504b4d8a8630?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=643&q=80',
            name: 'Radio FM City',
            hours: 'Mondays at 7:30PM',
            description: 'You’re always in time for the punchline with every kind of comedy, including Kevin Hart’s Laugh Out Loud Radio and the Netflix is a Joke channel, which broadcasts stand-up.',
            host_by: this.hosts,
            episodes: this.episodes,
        }

    ];

    constructor(private http: HttpClient) {
      this.updateDatas();
     }

    updateDatas(){
       this.emissions=this.getEmissions();
     }
    getEmissions(){
      console.log("data.service.ts -> getEmissions()");
      return [];
    }
    getHosts() {
        return this.hosts;
    }

    getHostsById(id: number) {
        return this.hosts.find((data: any) => data.id === id);
    }
    getShows() {
        return this.shows;
    }

    getShowsById(id: number) {
        return this.shows.find((data: any) => data.id === id);
    }
    getJeuxById(id:number) {
      console.log("getjeu");
      let url="http://app.clubpeinard.fr/connecteurs/jeux.php?action=list&id_part="+id;
      this.http.get(url).subscribe((res)=>{
        if (res){
          return res;
        }
        else {
          return false;
        }
      })
    }

}
