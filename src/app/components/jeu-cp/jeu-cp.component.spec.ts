import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JeuCpComponent } from './jeu-cp.component';

describe('JeuCpComponent', () => {
  let component: JeuCpComponent;
  let fixture: ComponentFixture<JeuCpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JeuCpComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JeuCpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
