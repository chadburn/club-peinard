# README #

Application club Peinard
Build with Ionic, capacitor and Angular

### How do I get set up? ###

# Install :
npm install
# Test
ionic serve

### App Build :
# IOS
ionic capacitor add ios
ionic capacitor open ios

# Android
ionic capacitor add android
ionic capacitor open android
