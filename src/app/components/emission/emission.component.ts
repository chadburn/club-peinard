import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { AudioPlayerService } from 'src/app/services/audio-player.service';
@Component({
  selector: 'app-emission',
  templateUrl: './emission.component.html',
  styleUrls: ['./emission.component.scss'],
})
export class EmissionComponent implements OnInit {

  @Input() file: any;

  constructor(private AudioPlayerService: AudioPlayerService) { }

  ngOnInit() {}

  playAudio(file){
    console.log(file);
    this.AudioPlayerService.setAudio(file.url,true);
    this.AudioPlayerService.title=file.titre;
    this.AudioPlayerService.text=file.texte;
    this.AudioPlayerService.photo=file.image;
  }
}
