import { Component, OnInit,Input, OnDestroy } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { HttpClient,HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit  {
  @Input() connected:boolean;
  login:any;
  returnMessage:any;
  adherent:any={};
  constructor(private http: HttpClient) { }

  ngOnInit() {
    let id=localStorage.getItem('idadh');
    if (id){
      console.log("connected");
      this.connected=true;
      let nom=localStorage.getItem('nomadh');
      let prenom=localStorage.getItem('prenomadh');
      this.adherent={id:id,nom:nom,prenom:prenom};
    }
  }
  removeUser(){
  	this.connected=false;
  	this.adherent={};
  	localStorage.removeItem('idadh');
  	localStorage.removeItem('nomadh');
  	localStorage.removeItem('prenomadh');
  }
  onSubmit(data) {
    console.log(data);
  	const headers = new HttpHeaders({
  		'Content-Type': 'application/x-www-form-urlencoded'
  	});
  	const postData: any = data;
  	let url="https://app.clubpeinard.fr/connecteurs/login/auth2.php?email="+data.email;
  	this.http
  		.post(
  			url,
  			postData
  		)
  		.subscribe(response => {
  			if (response['success']){
  				this.connected=true;
  				this.adherent=response['adherent'];
  				localStorage.setItem('idadh', this.adherent.id);
  				localStorage.setItem('nomadh', this.adherent.nom);
  				localStorage.setItem('prenomadh', this.adherent.prenom);
  			}
  			else {
  				this.returnMessage="Vous n'êtes pas inscrit au club peinard.";
  			}
  		});
    console.log(data);
  }

}
