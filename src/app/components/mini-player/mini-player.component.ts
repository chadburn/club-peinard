import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { PlayerPage } from 'src/app/pages/player/player.page';
import { AudioPlayerService } from 'src/app/services/audio-player.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-mini-player',
  templateUrl: './mini-player.component.html',
  styleUrls: ['./mini-player.component.scss'],
})
export class MiniPlayerComponent implements OnInit {

  flagPlay;

  constructor( public modalController: ModalController,
              private AudioPlayerService: AudioPlayerService ) {
  //this.flagPlay=!this.AudioPlayerService.isPlaying;
  console.log(this.AudioPlayerService.isPlaying);
  this.AudioPlayerService.getIsPlaying().subscribe(

        );

 }

  ngOnInit() {
    //this.AudioPlayerService.setAudio("http://str2.creacast.com/radio_peinard",false);

    this.AudioPlayerService.getIsPlaying().subscribe(
              flagPlay => {this.flagPlay = !flagPlay}
          );
  }

  async presentModal(info: any) {
    const modal = await this.modalController.create({
        component: PlayerPage,
        componentProps: { data: info }
    });
    return await modal.present();
  }

  toggleIconPlay() {
    //console.log(this.AudioPlayerService.isPlaying)
    //this.flagPlay = !this.flagPlay;
    this.AudioPlayerService.toggleAudio();
  }
  play() {
    //this.AudioPlayerService.loadAudio();
      //this.AudioPlayerService.setAudio("http://str2.creacast.com/radio_peinard",true);
      this.AudioPlayerService.playAudio();
      console.log("play audio");

  }

  pause() {
      this.AudioPlayerService.pauseAudio();

  }

}
