import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-reglement',
  templateUrl: './reglement.page.html',
  styleUrls: ['./reglement.page.scss'],
})
export class ReglementPage implements OnInit {

  	constructor( private modalController: ModalController ) { }

  ngOnInit() {
  }
  async closeModal() {
  this.modalController.dismiss();
}

}
