import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from '@angular/forms';
import { BigCardComponent } from "./big-card/big-card.component";
import { EpisodeListItemComponent } from "./episode-list-item/episode-list-item.component";
import { FavoriteItemComponent } from "./favorite-item/favorite-item.component";
import { HostItemComponent } from "./host-item/host-item.component";
import { NotificationItemComponent } from "./notification-item/notification-item.component";
import { IonicModule } from "@ionic/angular";
import { MiniPlayerComponent } from "./mini-player/mini-player.component";
import { EmissionComponent } from "./emission/emission.component";
import { JeuCpComponent } from "./jeu-cp/jeu-cp.component";
import { CpPubComponent } from "./cp-pub/cp-pub.component";
import { LoginFormComponent } from "./login-form/login-form.component";
import { InscriptionComponent } from "./inscription/inscription.component";
import { SwiperModule } from 'swiper/angular';

@NgModule({
  declarations: [
    BigCardComponent,
    EpisodeListItemComponent,
    FavoriteItemComponent,
    HostItemComponent,
    NotificationItemComponent,
    MiniPlayerComponent,
    EmissionComponent,
    JeuCpComponent,
    CpPubComponent,
    LoginFormComponent,
    InscriptionComponent
  ],
  imports: [CommonModule, IonicModule,FormsModule,SwiperModule],
  exports: [
    BigCardComponent,
    EpisodeListItemComponent,
    FavoriteItemComponent,
    HostItemComponent,
    NotificationItemComponent,
    MiniPlayerComponent,
    EmissionComponent,
    JeuCpComponent,
    CpPubComponent,
    LoginFormComponent,
    InscriptionComponent
  ]
})
export class ComponentsModule {}
